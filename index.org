#+OPTIONS: html-link-use-abs-url:nil html-postamble:nil html-preamble:nil html-scripts:nil html-style:nil html5-fancy:t
#+OPTIONS:  tex:t toc:nil num:t H:4
#+HTML_DOCTYPE: xhtml5
#+HTML_CONTAINER: div
#+DESCRIPTION:
#+KEYWORDS:
#+AUTHOR: Martin G. Skjæveland
#+TITLE: Practical Ontology Pattern Instantiation, Discovery, and Maintanence with Reasonable Ontology Templates
#+HTML_LINK_HOME:
#+HTML_LINK_UP:
#+HTML_MATHJAX:
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.ottr.xyz/inc/style.css" />
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.ottr.xyz/inc/spec.css" />
#+XXX-HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/atom-one-light.min.css"/>
#+HTML_HEAD: <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
#+XXX-HTML_HEAD: <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
#+HTML_HEAD: <script type="application/javascript" src="https://www.ottr.xyz/inc/toc.js"></script>
#+HTML_HEAD_EXTRA:
#+SUBTITLE:
#+INFOJS_OPT:
#+LATEX_HEADER:

#+BEGIN_EXPORT html
<div id="left">
<p>
  <a href="#top">top</a>
  <br/>
  examples:
  <span class="button" onclick="$('.examplebox').show()">show</span>
  /
  <span class="button" onclick="$('.examplebox').hide()">hide</span>
</p>
  <h4>Table of Contents</h4>
  <div id="toc"></div>
</div>
#+END_EXPORT

#+BEGIN_EXPORT html
<div id="right">
  <a name="top"></a>
  <h1 class="title">Practical Ontology Pattern Instantiation, Discovery, and Maintanence with Reasonable Ontology Templates</h1>
  <img id="logo" alt="OTTR" src="https://www.ottr.xyz/logo/OTTR.jpg"/>
  <p><b>
    Online companion demonstration to ISWC 2018 paper.
  </b></p>
#+END_EXPORT

#+HTML: <div id="toc-content">

* Introduction

  The demo will show how templates and instances are specifies - using
  different formats, the result of their expansion, and how templates
  may be analysed for different imperfections.

The source of this demo is available at the open git repository
https://gitlab.com/ottr/pub/iswc2018-demo. It contains:
 - the OTTR templates and OTTR template instances found on this page
 - a script that
   - downloads the Java jar executables necessary to run the demo locally
   - executes all the executable examples found on this page
 - this page
 - the org-mode source file for all the above

** OTTR Serialisations

   In this demonstration we will show OTTR templates and instances
   using different serialisations:

   - stOTTR
   - wOTTR
   - tabOTTR

*** stOTTR

    stOTTR is a compact way of representing templates and instances
    that is also easy to read and write.  A BNF specification of the
    language is available at
    https://gitlab.com/ottr/language/stOTTR/blob/master/stOTTR-BNF.md

*** wOTTR

    To enable truly practical use of OTTR for OWL ontology engineering,
    we have developed a special-purpose RDF/OWL vocabulary, called
    /wOTTR/, with which OTTR templates can be formulated. This has the
    benefit that we can leverage the existing stack of W3C languages
    and tools for developing, publishing, and maintaining templates.
    The wOTTR format supports writing Triple Template instances as
    regular RDF triples. This means that a pattern represented by an
    RDF graph or RDF/OWL ontology can easily be turned into an OTTR
    template by simply specifying the name of the template and its
    parameters with the wOTTR vocabulary.  Furthermore, this means that
    we can make use of existing ontology editors and reasoners to
    construct and verify the soundness of templates.  The wOTTR
    representation has been developed to closely resemble stOTTR. It
    uses RDF resources to represent parameters and arguments, and RDF
    lists (which have a convenient formatting in Turtle syntax) for
    lists of parameters and arguments.  The vocabulary is published at
    http://ns.ottr.xyz.

*** tabOTTR

    tabOTTR is developed particularly for representing large sets
    template instances in tabular formats such as spreadsheets, and is
    intended for domain expert use.  A specification of tabOTTR is
    available at https://gitlab.com/ottr/language/tabOTTR/builds/artifacts/develop/file/tabOTTR.html?job=build-job

** OTTR library and online web application

   The "official" OTTR template repository at http://library.ottr.xyz
   contains OTTR templates for expressing common RDF, RDFS and OWL
   patterns, including other example templates.

   These templates are conveniently presented in an online library
   that is linked to the online web application

   The web application displays the template, including visualisations
   of the pattern, links to dependant templates, and different formats
   which are generated from the template (some are experimental):
   - different expansions
   - SPARQL queries (SELECT, CONSTRUCT, UPDATE)
   - XSD+SAWSDL format
   - XML sample

** Prefixes

 In all the example code blocks the following prefixes are used.

#+NAME: prefixes
#+BEGIN_SRC n3 
### standard
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix owl:  <http://www.w3.org/2002/07/owl#> .
@prefix xsd:  <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

### ottr prefixes:
@prefix ottr: <http://ns.ottr.xyz/templates#> .

@prefix t-owl-axiom: <http://candidate.ottr.xyz/owl/axiom/> .
@prefix t-owl-rstr: <http://candidate.ottr.xyz/owl/restriction/> .

### examples:
@prefix x1:     <http://candidate.ottr.xyz/owl/axiom/SubClassOf#> .
@prefix x2:     <http://candidate.ottr.xyz/rdfs/ResourceDescription#> .

@prefix ex:     <http://example.org#> .
#+END_SRC


* Templates and Instances

An OTTR template consists of a head and a body.  The body represents
a parameterised ontology pattern, and the head specifies the
template's name and its parameters.

A template instance consists of a template name and a list of
arguments that matches the parameters of the designated template, and
represents a replica of the template's body pattern where parameters
are replaced by the instance's arguments.

The template body comprises only template instances, i.e., the
template pattern is recursively built up from other templates, under
the constraint that cyclic template dependencies are not allowed.

There is one special base template, the Triple template, which takes
three arguments.  This template has no body but represents a single
RDF triple in the obvious way.

Expanding an instance is the process of recursively replacing
instances with the pattern they represent. This process terminates
with an expression containing only Triple template instances, hence
representing an RDF graph.



#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Simple template</h4>

The SubClassOf template is a simple representation of the
~rdfs:subClassOf~ relationship.  It has two parameters ~x1:SubClass~
and ~x1:SuperClass~, the resources representing the parameter
variables may be given as a list. The code snippet below contains only
the head of the template.

#+BEGIN_SRC n3
t-owl-axiom:SubClassOf a ottr:Template ;
   ottr:withVariables ( x1:SubClass x1:SuperClass ) .
#+END_SRC

The body of a template may in the RDF serialisation given as either
template instances or as regular RDF triples. Below, on the left, the
template is given with a body containing a single instance of a Triple
template. On the right, the equivalent representation using regular
RDF triples is used.

#+BEGIN_sidebyside
#+BEGIN_sidebyside2
#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/intro/SubClassOf1 
<<prefixes>>
t-owl-axiom:SubClassOf a ottr:Template ;
   ottr:withVariables ( x1:SubClass x1:SuperClass ) .

[] ottr:templateRef <http://candidate.ottr.xyz/rdf/Triple> ;
   ottr:withValues ( x1:SubClass rdfs:subClassOf x1:SuperClass ) .
#+END_SRC
#+END_sidebyside2
#+BEGIN_sidebyside2
#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/intro/SubClassOf2
<<prefixes>>
t-owl-axiom:SubClassOf a ottr:Template ;
   ottr:withVariables ( x1:SubClass x1:SuperClass ) .

x1:SubClass rdfs:subClassOf x1:Superclass .
#+END_SRC
#+END_sidebyside2
#+END_sidebyside
#+end_examplebox

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Simple template instances</h4>

Instances of templates are given just as in templates bodies.

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/intro/SubClassOf1Instances 
<<prefixes>>

[] ottr:templateRef t-owl-axiom:SubClassOf ;
   ottr:withValues ( ex:Pizza ex:Food ) .
[] ottr:templateRef t-owl-axiom:SubClassOf ;
   ottr:withValues ( ex:Burger ex:Food ) .
[] ottr:templateRef t-owl-axiom:SubClassOf ;
   ottr:withValues ( ex:FishSoup ex:Food ) .
#+END_SRC
#+END_examplebox


* Lutra

Lutra is our Java implementation of the OTTR template macro expander
is available as open source with an LGPL licence at
http://gitlab.com/ottr/lutra/lutra.  It can read and write templates
and instances on the formats represented above and expand them into
RDF graphs and OWL ontologies.

#+NAME: sh-download
#+BEGIN_SRC sh :exports none
if [ ! -f ./lutra.jar ];
then
  wget https://gitlab.com/ottr/lutra/lutra/builds/artifacts/v0.1.6/raw/lutra.jar?job=build_tags -O lutra.jar
fi

if [ ! -f ./findPatterns.jar ];
then
  wget http://folk.uio.no/martige/what/20180601/findPatterns.jar -O findPatterns.jar
fi
#+END_SRC

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">The Lutra CLI</h4>
This is the command line interface of Lutra.
#+NAME: sh-cli
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar
#+END_SRC
#+END_examplebox

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Converting to stOTTR serialisation</h4>

Lutra can convert templates and template instances serialised as RDF
into the stOTTR serialisation.

#+NAME: sh-stottr
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -stottr -in tpl/intro/SubClassOf1
#+END_SRC
#+END_examplebox

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Expanding simple template instances</h4>

Lutra can expand template instances.

#+NAME: sh-sub-inst-expansion
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -in inst/intro/SubClassOf1Instances
#+END_SRC
#+END_examplebox

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Expanding Template definition</h4>

Note also that templates can be expanded, i.e., expanding the
instances in the body, hence resulting in a prototype of the pattern
the template represents.

#+NAME: sh-expansion
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -in tpl/intro/SubClassOf1
#+END_SRC
#+END_examplebox

* Parameter Types and Type checking

The type of the parameter specifies the permissible type of its
arguments. The available types are limited to a specified set of
classes and datatypes defined in the XSD, RDF, RDFS, and OWL
specifications, e.g,. ~xsd:integer~, ~rdf:Property~, ~rdfs:Resource~
and ~owl:ObjectProperty~.

The OWL ontology available at http://ns.ottr.xyz/templates-term-types.owl
declares all permissible types and organises them in a hierarchy of
/subtypes/ and /incompatible/ types, e.g.,
~owl:ObjectProperty~ is a subtype of ~rdf:Property~ and ~xsd:integer~
and ~rdf:Property~ are incompatible.  The most general and default
type is ~rdfs:Resource~. 

This information is used to type check template instantiations; a
parameter may not be instantiated by an argument with an incompatible
type.

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Declaring parameter types</h4>

Parameter types are specified by using dedicated properties. In order
to do so, the parameters of the templates must be explicitly
represented (here as blank nodes), and are uniquely order with an
index.

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/types/SubClassOf3.ttl
<<prefixes>>
t-owl-axiom:SubClassOfTest1 a ottr:Template ;
   ottr:hasParameter 
     [ ottr:index 1 ; ottr:classVariable x1:SubClass ] ,
     [ ottr:index 2 ; ottr:classVariable x1:SuperClass ] .

[] ottr:templateRef <http://candidate.ottr.xyz/rdf/Triple> ;
   ottr:withValues ( x1:SubClass rdfs:subClassOf x1:SuperClass ) .
#+END_SRC

This is the stOTTR representation of the template:

#+NAME: sh1
#+BEGIN_SRC sh :exports results :results output
java -jar lutra.jar -stottr -in tpl/types/SubClassOf3.ttl -out tpl/types/SubClassOf3.ttl.stottr
cat tpl/types/SubClassOf3.ttl.stottr
#+END_SRC

Instances for templates with explicitly typed parameters are given as usual:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/types/SubClassOf3Instances
<<prefixes>>
[] ottr:templateRef t-owl-axiom:SubClassOfTest1 ;
   ottr:withValues ( ex:Pizza ex:Food ) .
#+END_SRC

Expanding the instance. To load the local template defined above, we
use the ~-lib~ option with the folder of the templates to be loaded as
argument. All templates, including subfolders (recursively), are
loaded.

A warning is given when loading the local template since its IRI is
different than it location.

#+NAME: sh-warning
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -lib tpl/types -in inst/types/SubClassOf3Instances -out inst/types/SubClassOf3Instances.out
#+END_SRC

This is the result of the expansion:

#+NAME: sh2
#+BEGIN_SRC sh :exports results :results output
cat inst/types/SubClassOf3Instances.out
#+END_SRC

An instance of the template with arguments that are incompatible with
the type of the parameters will generate an error. In the following
template instance the first argument is a literal, which cannot
represent an OWL class.

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/types/SubClassOf3Instances2
<<prefixes>>
[] ottr:templateRef t-owl-axiom:SubClassOfTest1 ;
   ottr:withValues ( "PizzaLiteral" ex:Food ) .
#+END_SRC

Expanding this instance gives the following exception:

#+BEGIN_quote
Type error, expecting non literal for class variable, but found literal: PizzaLiteral
#+END_quote

#+NAME: sh-error
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -lib tpl/types -in inst/types/SubClassOf3Instances2
#+END_SRC
#+END_examplebox

* Cardinalities

The cardinality of a parameter specifies the number of required
arguments to the parameter.  There are four cardinalities: 
 - /mandatory/
 - /optional/ 
 - /multiple/
 - /optional multiple/, which is shorthand for /mandatory/ and
   /optional/ combined.

/Mandatory/ is the default cardinality.  Mandatory parameters require
an argument, otherwise the instance is illegal.

Optional parameters permit a missing value; ~ottr:none~ designates
this value.  If ~ottr:none~ is an argument to a mandatory parameter of
an instance, the instance is ignored and will not be included in the
expansion.

A parameter with cardinality /multiple/ requires a list as argument.

Parameters with cardinality /optional multiple/ also
accept /ottr:none/ as a value and behave like optional arguments.

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Optional parameters</h4>

The following template is handy for expressing RDFS annotations on resources. All parameters, except the mandatory ~resource~ parameter are optional.

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/card/ResourceDescription
<<prefixes>>
<http://candidate.ottr.xyz/rdfs/ResourceDescription> a ottr:Template ;
    ottr:hasParameter
        [ ottr:index 1;  ottr:nonLiteralVariable  x2:resource ] ,
	[ ottr:index 2;  ottr:literalVariable     "label";      ottr:optional true ] ,
	[ ottr:index 3;  ottr:literalVariable     "comment";    ottr:optional true ] ,
	[ ottr:index 4;  ottr:variable            x2:seeOther;  ottr:optional true ] ,
	[ ottr:index 5;  ottr:variable            x2:defOther;  ottr:optional true ] .

x2:resource
    rdfs:label        "label" ;
    rdfs:comment      "comment" ;
    rdfs:seeAlso      x2:seeOther ;
    rdfs:isDefinedBy  x2:defOther .
#+END_SRC

Example instances:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/card/ResourceDescriptionInstances
<<prefixes>>
[] ottr:templateRef <http://candidate.ottr.xyz/rdfs/ResourceDescription> ;
   ottr:withValues ( ex:thing1 "label1" "comment1" ottr:none ottr:none ) .

[] ottr:templateRef <http://candidate.ottr.xyz/rdfs/ResourceDescription> ;
   ottr:withValues ( ex:thing2 "label2" ottr:none ex:seeOther2 ex:defOther2  ) .

[] ottr:templateRef <http://candidate.ottr.xyz/rdfs/ResourceDescription> ;
   ottr:withValues ( ex:thing3 ottr:none ottr:none ottr:none ottr:none ) .
#+END_SRC

This is the result of expanding these instances. ~ex:thing3~ does not
occur in the output since all triples are removed.

#+NAME: sh-opt-inst-expansion
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -in inst/card/ResourceDescriptionInstances
#+END_SRC
#+END_examplebox

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Parameters with cardinality multiple</h4>

The /multiple/ cardinality is useful for expression that naturally
should accept multiple values.  The ~EquivObjectUnionOf~ template expresses that a class
is equivalent to a union of classes as according to the OWL specification.

The template directly depends on, i.e,. its body contains instances
of, other templates which are available at their IRI address. Lutra
fetches these template definitions on expansion.

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/card/EquivObjectUnionOf
<<prefixes>>
t-owl-axiom:EquivObjectUnionOf a ottr:Template ;
    ottr:hasParameter :pRestriction, :pList .

:pRestriction   ottr:index 1;  ottr:classVariable  :xRestriction .
:pList          ottr:index 2;  ottr:listVariable    ( :item1 :item2 ) .

[] ottr:templateRef t-owl-axiom:EquivalentClass ;
   ottr:withValues ( :xRestriction _:restriction ) .

[] ottr:templateRef t-owl-rstr:ObjectUnionOf ;
    ottr:withValues ( _:restriction ( :item1 :item2 ) ) .
#+END_SRC

An example instance:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle inst/card/EquivObjectUnionOfInstances
<<prefixes>>
[] ottr:templateRef t-owl-axiom:EquivObjectUnionOf ;
   ottr:withValues ( ex:MyFood ( ex:Pizza ex:Burger ex:FishSoup) ) .
#+END_SRC

This expands to:

#+NAME: sh-multi-inst-expansion
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -in inst/card/EquivObjectUnionOfInstances
#+END_SRC
#+END_examplebox

* Expansion Modes

Instances of templates that accept list arguments may be used together
with an expansion /mode/.  The mode indicates that the list arguments
will in the expansion be used to generate multiple instances of the
template.

The instances to be generated are calculated by temporarily
considering all arguments to the instance as lists, where single value
arguments become singular lists.  Currently only one mode, cross mode,
is implemented, but others are possible.  In cross mode, one instance
per element in the cross product of the temporary lists is generated

List arguments used without an expansion mode behave just like regular
arguments.  

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Expansion mode</h4>

To illustrate the effect of using expansion mode, we introduce three
different variants of the ~SubClassOf~ template and show the results
of an example instance.


~SubClassOfTest1~ accepts a list of superclasses:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/mode/SubClassOf1.ttl
<<prefixes>>
t-owl-axiom:SubClassOfTest1 a ottr:Template ;
   ottr:hasParameter 
     [ ottr:index 1 ; ottr:classVariable x1:SubClass ] ,
     [ ottr:index 2 ; ottr:listVariable ( x1:SuperClasses ) ] .

[] ottr:templateRef t-owl-axiom:SubClassOf ;
   ottr:hasArgument
	[ ottr:index 1; ottr:value x1:SubClass ] ,
	[ ottr:index 2; ottr:eachValue ( x1:SuperClasses ) ] .
#+END_SRC

Example instance:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/mode/SubClassOf1Instances
<<prefixes>>
[] ottr:templateRef t-owl-axiom:SubClassOfTest1 ;
   ottr:withValues ( ex:MyFood ( ex:Pizza ex:Burger ex:FishSoup) ) .
#+END_SRC

Expansion:

#+NAME: sh-mode-inst-expansion1
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -quiet -expand -lib tpl/mode -in tpl/mode/SubClassOf1Instances
#+END_SRC

~SubClassOfTest2~ accepts a list of subclasses:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/mode/SubClassOf2.ttl
<<prefixes>>
t-owl-axiom:SubClassOfTest2 a ottr:Template ;
   ottr:hasParameter 
     [ ottr:index 1 ; ottr:listVariable ( x1:SubClasses ) ] ,
     [ ottr:index 2 ; ottr:classVariable x1:SuperClass ] .

[] ottr:templateRef t-owl-axiom:SubClassOf ;
   ottr:hasArgument
	[ ottr:index 1; ottr:eachValue ( x1:SubClasses ) ] ,
	[ ottr:index 2; ottr:value x1:SuperClass ] .
#+END_SRC

Example instance:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/mode/SubClassOf2Instances
<<prefixes>>
[] ottr:templateRef t-owl-axiom:SubClassOfTest2 ;
   ottr:withValues ( ( ex:Tropical ex:Pepperoni ex:Grandiosa ) ex:Pizza ) .
#+END_SRC

Expansion:

#+NAME: sh-mode-inst-expansion2
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -quiet -expand -lib tpl/mode -in tpl/mode/SubClassOf2Instances
#+END_SRC

 
~SubClassOfTest3~ accepts a list of subclasses and a a list of superclasses

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/mode/SubClassOf3.ttl
<<prefixes>>
t-owl-axiom:SubClassOfTest3 a ottr:Template ;
   ottr:hasParameter 
     [ ottr:index 1 ; ottr:listVariable ( x1:SubClasses ) ] ,
     [ ottr:index 2 ; ottr:listVariable ( x1:SuperClasses ) ] .

[] ottr:templateRef t-owl-axiom:SubClassOf ;
   ottr:hasArgument
	[ ottr:index 1; ottr:eachValue ( x1:SubClasses ) ] ,
	[ ottr:index 2; ottr:eachValue ( x1:SuperClasses ) ] .
#+END_SRC

Example instance:

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/mode/SubClassOf3Instances
<<prefixes>>
[] ottr:templateRef t-owl-axiom:SubClassOfTest3 ;
   ottr:withValues ( ( ex:A1 ex:A2 ex:A3) ( ex:B1 ex:B2 ex:B3 ) ) .
#+END_SRC

Expansion:

#+NAME: sh-mode-inst-expansion3
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -quiet -expand -lib tpl/mode -in tpl/mode/SubClassOf3Instances
#+END_SRC
#+END_examplebox

* Combining features: NamedPizza

This section illustrates the example used in the demo paper, the
~NamedPizza~ template

It is available at its IRI:
http://draft.ottr.xyz/pizza/NamedPizza. This page displays the
template in different serialisations, lists its dependencies,
visualises its prototypical pattern, and provides links to other
available formats that can be generated from an OTTR template.

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">NamedPizza</h4>

This is the wOTTR serialisation of the ~NamedPizza~ template. 

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/pizza/NamedPizza.ttl
<<prefixes>>
<http://draft.ottr.xyz/pizza/NamedPizza> a ottr:Template ;
  ottr:hasParameter
     [ ottr:index 1 ; ottr:classVariable :pizza ] ,
     [ ottr:index 2 ; ottr:individualVariable :country; ottr:optional true ] ,
     [ ottr:index 3 ; ottr:listVariable (:toppings) ]  .

[] ottr:templateRef <http://candidate.ottr.xyz/owl/axiom/SubClassOf> ;
   ottr:withValues ( :pizza p:NamedPizza ) .

[] ottr:templateRef <http://candidate.ottr.xyz/owl/axiom/SubObjectSomeValuesFrom> ;
   ottr:hasArgument
	[ ottr:index 1; ottr:value :pizza ] ,
	[ ottr:index 2; ottr:value p:hasTopping ] ,
	[ ottr:index 3; ottr:eachValue (:toppings) ] .

[] ottr:templateRef <http://candidate.ottr.xyz/owl/axiom/SubObjectHasValue> ;
   ottr:withValues ( :pizza p:hasCountryOfOrigin :country ) .

[] ottr:templateRef <http://candidate.ottr.xyz/owl/axiom/SubObjectAllValuesFrom> ;
   ottr:withValues ( :pizza p:hasTopping _:alltoppings ) .

[] ottr:templateRef <http://candidate.ottr.xyz/owl/restriction/ObjectUnionOf> ;
   ottr:withValues ( _:alltoppings (:toppings) ) .
#+END_SRC

Its stOTTR serialisation; note that we are reading the template
definition from its IRI, where the template is published.

#+NAME: sh-pizza1
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -stottr -in http://draft.ottr.xyz/pizza/NamedPizza
#+END_SRC

Example instance containing three pizzas. Note that the file
specifies that it is an OWL ontology.

#+BEGIN_SRC n3 :noweb strip-export :mkdirp yes :tangle tpl/pizza/NamedPizzaInstances
<<prefixes>>
[] a owl:Ontology .

[] ottr:templateRef <http://draft.ottr.xyz/pizza/NamedPizza> ;
   ottr:withValues ( ex:Grandiosa ottr:none ( ex:Tomato ex:Jarlsberg ex:Ham ex:SweetPepper ) ) .
[] ottr:templateRef <http://draft.ottr.xyz/pizza/NamedPizza> ;
   ottr:withValues ( ex:Margherita ex:Italy ( ex:Tomato ex:Mozzarella ) ) .
[] ottr:templateRef <http://draft.ottr.xyz/pizza/NamedPizza> ;
   ottr:withValues ( ex:Tropical ex:Hawaii ( ex:Tomato ex:Cheese ex:Pineapple ex:Ham ) ) .
#+END_SRC

The result of the expansion.  The occurrence of the ~[] a
owl:Ontology~ triple in the input, causes Lutra to render the output
using the OWLAPI; this can be disabled with the ~-noOWLOutput~ flag.

#+NAME: sh-pizza2
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -expand -in tpl/pizza/NamedPizzaInstances
#+END_SRC
#+END_examplebox

** tabOTTR

   The tabOTTR format is useful for compactly giving multiple
   instances of templates, and makes OTTR available to users who
   prefer working with tabular formats such as spreadsheets.

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">NamedPizza tabular input</h4>

The code block below is copy of one of the sheets in the Excel
spreadsheet at ~tab/NamedPizza-instances.xlsx~ (available in the git
repo: https://gitlab.com/ottr/pub/iswc2018-demo).

#+BEGIN_SRC n3
#OTTR	prefix	
owl	http://www.w3.org/2002/07/owl#	
rdf	http://www.w3.org/1999/02/22-rdf-syntax-ns#	
xsd	http://www.w3.org/2001/XMLSchema#	
rdfs	http://www.w3.org/2000/01/rdf-schema#	
ottr	http://ns.ottr.xyz/templates#	
	http://example.com#	
#OTTR	end	
		
#OTTR	template	http://draft.ottr.xyz/pizza/NamedPizza
Pizza	Country	Toppings
1	2	3
iri	iri	iri+
:Margherita	:Italy	:Tomato|:Mozzarella
:Grandiosa	:Norway	:Tomato|:Jarlsberg|:Ham|:SweetPepper
:Hawaii		:Tomato|:Cheese|:Ham|:PineApple
:BigOne		:Tomato|:Cheese|:Ham
#OTTR	end
#+END_SRC

Lutra can expand the instances represented in tabOTTR format in the Excel file:

#+NAME: sh-pizza-tab
#+BEGIN_SRC sh :exports both :results output
java -jar lutra.jar -tabottr -in tab/NamedPizza-instances.xlsx
#+END_SRC
#+END_examplebox



** Generated formats from OTTR templates

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Other formats from templates</h4>

This example illustrates some of the formats that may be generated
from the ~NamedPizza~ template.

*SPARQL SELECT* query of pattern

#+BEGIN_SRC
SELECT  *
WHERE
  { _:b0      owl:allValuesFrom   _:b3 ;
              owl:onProperty      p:hasTopping ;
              rdf:type            owl:Restriction .
    _:b1      owl:hasValue        ?param2 ;
              owl:onProperty      p:hasCountryOfOrigin ;
              rdf:type            owl:Restriction .
    _:b2      owl:onProperty      p:hasTopping ;
              owl:someValuesFrom  ?param3item ;
              rdf:type            owl:Restriction .
    _:b3      owl:unionOf         ?param3 ;
              rdf:type            owl:Class .
    ?param1   rdfs:subClassOf     _:b0 ;
              rdfs:subClassOf     _:b1 ;
              rdfs:subClassOf     _:b2 ;
              rdfs:subClassOf     p:NamedPizza .
    ?param3item  rdf:type         owl:Class .
    p:hasCountryOfOrigin
              rdf:type            owl:ObjectProperty .
    p:hasTopping  rdf:type        owl:ObjectProperty .
    ?param3  (rdf:rest)*/rdf:first ?param3item
  }
#+END_SRC

Also available at
http://osl.ottr.xyz/lifting/select/?tpl=http://draft.ottr.xyz/pizza/NamedPizza

*XSD+SAWSDL* description of template head:

Available at http://osl.ottr.xyz/format/head/xml/?tpl=http://draft.ottr.xyz/pizza/NamedPizza

*XML* sample of template head:

Available at http://osl.ottr.xyz/sample/head/xml/?tpl=http://draft.ottr.xyz/pizza/NamedPizza
#+END_examplebox

* Finding Redundancies

We here present redundancies in template libraries
and our method of finding and fixing them.
We here work with two types of redundancy: a lack of reuse of existing
templates and recurring patterns not captured by templates
within the library. If the body of a template ~T~ is a subset
of another template ~R~'s body, assuming an appropriate substitution of the
variables of ~T~, then ~R~ has a lack of reuse of ~T~.
On the other hand, a pattern of template instances might occur
across multiple templates without there being any template having this
pattern as a body. This type of redundancy is called an uncaptured pattern.
A lack of reuse can be fixed by simply substituting the offending instances
in ~R~ by an appropriate instance of ~T~. To fix a redundancy
resulting from an uncaptured pattern, we first have to introduce a new template
that has the recurring pattern as its body and then refactor out the redundancy.

A naive method for finding the two types of redundancy based on direct unification
of subsets of the template's bodies is infeasible for large template libraries.
We have therefore developed an efficient method for finding lack of reuse and uncaptured patterns,
which over-approximates the results of unification
based on the notion of a dependency pair, which intuitively captures repeated
use of templates without considering parameters.
A dependency pair is a pair of a multiset of templates ~I~ and a set of templates ~T~,
such that all templates in ~T~ have at least as many occurences of each
template in ~I~ as they occur in ~I~ in its body.
The idea is that ~I~ then describes a pattern that is used in the templates ~T~.
In order to also detect patterns containing different ~Triple~ instances, we
treat a ~Triple~ instance ~(s, p, o)~ as an instance of the form ~p(s,o)~ and thus ~p~ as a template.

For each template ~t~ in ~T~ in each dependency pair ~(I, T)~ we can construct a suggestion of a new template ~s~
by extracting the instances in ~t~'s body corresponding to the instances in ~I~,
making all parameters
and constants (except blank nodes only occuring in the pattern) in this set of instances a parameter. 
For each such template ~s~ we have one of three cases: 
(1) the body of this template does not represent a repeated pattern in the other templates of ~T~;
(2) ~s~ is equal to an already existing template, in which case we have found
an instance of lack of reuse in all the other template of ~T~ which
shares the pattern;
(3) we have found an uncaptured pattern represented which is remedied by the introduction of ~s~.

The following examples demonstrates the method for finding
redundancies in a template library. The template library used in the
example is found in the folder ~redundancy-templates/~.

The analysis program is not available as open source and is therefore
packaged in a separate jar file.

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Analysis of example patterns</h4>

In the following example, only the patterns from the above mentioned paper is presented.

The program first outputs the template definitions loaded into the
library.  Then it prints each dependency pair found from the loaded templates; here is an example:

#+BEGIN_Example
Pattern
	t-owl-axiom:SubClassOf:1
	t-owl-axiom:SubObjectSomeValuesFrom:1
	t-owl-axiom:SubObjectAllValuesFrom:1
	t-owl-rstr:ObjectUnionOf:1
occurs in
	http://example.org/Burger
	http://draft.ottr.xyz/pizza/NamedPizza
#+END_Example

This dependency pair shows that both the templates ~pz:NamedPizza~ and ~ex:Burger~ contain
one instance (denoted by the trailing integer ~:1~) of each of the
templates ~t-owl-axiom:SubClassOf~, ~t-owl-axiom:SubObjectSomeValuesFrom~,
~t-owl-axiom:SubObjectAllValuesFrom~ and ~t-owl-rstr:ObjectUnionOf~.

The dependency pair is followed by the
template suggestions constructed from that dependency pair. Here it
removes duplicate suggestions via unification of templates. The suggested template from this dependency pair is:

#+BEGIN_Example
[name68_0](:subject, :hasCondiment, <:condiments>, ex:Burger) ::
    x | t-owl-axiom:SubObjectSomeValuesFrom(:subject, :hasCondiment, <:condiments>)
    t-owl-axiom:SubClassOf(:subject, ex:Burger)
    t-owl-rstr:ObjectUnionOf(25134aae:163da50f911:-7f93, <:condiments>)
    t-owl-axiom:SubObjectAllValuesFrom(:subject, :hasCondiment, 25134aae:163da50f911:-7f93)
#+END_Example

Finally,
if there are any lack of reuse captured by this dependency pair, these
are reported:

#+BEGIN_Example
http://example.org/Burger has a lack of reuse of http://draft.ottr.xyz/pizza/NamedPizza
#+END_Example

Below is the complete output from analysing the templates:

#+NAME: sh-findPatterns-p
#+BEGIN_SRC sh :exports both :results output
java -jar findPatterns.jar -p redundancy-templates/
#+END_SRC
#+END_examplebox

#+BEGIN_examplebox
#+HTML: <h4 class="example-title">Analysis of all patterns</h4>

In this example we present all redundancies in the entire library.

#+NAME: sh-findPatterns-a
#+BEGIN_SRC sh :exports both :results output
java -jar findPatterns.jar -a redundancy-templates/
#+END_SRC
#+END_examplebox

* Script

  All the executable examples may be run with the script ~run.sh~, this is its contents:

#+BEGIN_SRC sh :exports code :noweb yes :tangle run.sh
## write commands too
set -x
printf "\n\n\n"


## download jar files
<<sh-download>>
printf "\n\n\n"


## display CLI
<<sh-cli>>
printf "\n\n\n"


## convert to stOTTR serialisation
<<sh-stottr>>
printf "\n\n\n"


## expand instances
<<sh-sub-inst-expansion>>
printf "\n\n\n"


## expand template definition
<<sh-expansion>>
printf "\n\n\n"


## types: stOTTR serialisation
<<sh1>>
printf "\n\n\n"


## type check
<<sh-warning>>
printf "\n\n\n"


## type expansion
<<sh2>>
printf "\n\n\n"


## type error
<<sh-error>>
printf "\n\n\n"


## cardinality: optional
<<sh-opt-inst-expansion>>
printf "\n\n\n"


## cardinality: multiple
<<sh-multi-inst-expansion>>
printf "\n\n\n"


## mode test1
<<sh-mode-inst-expansion1>>
printf "\n\n\n"


## mode test2
<<sh-mode-inst-expansion2>>
printf "\n\n\n"


## mode test3
<<sh-mode-inst-expansion3>>
printf "\n\n\n"


## pizza stottr
<<sh-pizza1>>
printf "\n\n\n"


## pizza instances
<<sh-pizza2>>
printf "\n\n\n"


## pizza tabOTTR
<<sh-pizza-tab>>
printf "\n\n\n"


## find redundancies, restricted to demo-paper patterns
<<sh-findPatterns-p>>
printf "\n\n\n"


## find redundancies, all
<<sh-findPatterns-a>>
printf "\n\n\n"
#+END_SRC

#+HTML: </div> <!-- end toc-content -->
#+HTML: </div> <!-- end right -->
