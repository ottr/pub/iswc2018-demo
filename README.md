# ISWC 2018 demo companion

Published at http://ottr.xyz/event/2018-10-08-iswc/

Contents:
- index.org: org-mode source file 
- run.sh: script for downloading and running executable examples
- tpl: example templates
- inst: example template instances
- tab: tabular template instances in spreadsheet
- redundancy-template: example templates for demonstrating redundancy analysis

The demo can be run with `./run.sh`. This requires Java 8 runtime.

The template files and instances can be generated from the index.org file with `make build`. This requires a reasonable recent emacs installation.
