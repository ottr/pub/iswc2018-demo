#+NAME: tplinst
#+BEGIN_SRC python :var template="IRI" :var instances=toppings :exports none
output = ""
output += "@prefix ottr: <http://ns.ottr.xyz/templates#> .\n"
output += "@prefix p:    <http://www.co-ode.org/ontologies/pizza/pizza.owl#> .\n"
for row in instances[1:]: # skip first row which contains table formatting instructions.
  output += "[ ottr:templateRef ";
  output += template;
  output += ' ; ottr:withValues ( ' ;
  for cell in row:
    if cell == '':
      output += 'ottr:none'
    else:
      output += str(cell)
    output += ' '
  output += ') ] .\n'
return output
#+END_SRC

#+BEGIN_SRC sparql :tangle test.rq
SELECT *
{ ?s ?p ?o }
#+END_SRC

#+BEGIN_SRC txt :tangle namedpizza.rq
PREFIX  :     <http://draft.ottr.xyz/pizza/NamedPizza#>
PREFIX  eg:   <http://www.example.org/>
PREFIX  t-rdf: <http://candidate.ottr.xyz/rdf/>
PREFIX  owl:  <http://www.w3.org/2002/07/owl#>
PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  vcard: <http://www.w3.org/2001/vcard-rdf/3.0#>
PREFIX  t-owl-rstr: <http://candidate.ottr.xyz/owl/restriction/>
PREFIX  p:    <http://www.co-ode.org/ontologies/pizza/pizza.owl#>
PREFIX  rss:  <http://purl.org/rss/1.0/>
PREFIX  t-owl-axiom: <http://candidate.ottr.xyz/owl/axiom/>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  ottr: <http://ns.ottr.xyz/templates#>
PREFIX  ja:   <http://jena.hpl.hp.com/2005/11/Assembler#>
PREFIX  t-rdfs: <http://candidate.ottr.xyz/rdfs/>
PREFIX  t-owl-atom: <http://candidate.ottr.xyz/owl/atom/>
PREFIX  dc:   <http://purl.org/dc/elements/1.1/>

SELECT  ?param1 ?param2 (concat("(", concat(GROUP_CONCAT (?param3item; SEPARATOR=' '), ")")) AS ?toppings)
WHERE
  { ?param1   rdfs:subClassOf     [  owl:onProperty      p:hasTopping ;
                                     owl:someValuesFrom  ?param3item ;
                                     rdf:type            owl:Restriction ] ;
              rdfs:subClassOf     [  owl:allValuesFrom   [ owl:unionOf         ?param3 ] ;
                                     owl:onProperty      p:hasTopping ;
                                     rdf:type            owl:Restriction ] ;
              rdfs:subClassOf     p:NamedPizza .

OPTIONAL {
    ?param1   
              rdfs:subClassOf     [
          owl:hasValue        ?param2 ;
              owl:onProperty      p:hasCountryOfOrigin ;
              rdf:type            owl:Restriction ]
}


    ?param3  (rdf:rest)*/rdf:first ?param3item
  }
GROUP BY ?param1 ?param2
#+END_SRC

#+BEGIN_SRC txt :tangle toppings_with_comments.rq
PREFIX  :     <http://draft.ottr.xyz/pizza/NamedPizza#>
PREFIX  eg:   <http://www.example.org/>
PREFIX  t-rdf: <http://candidate.ottr.xyz/rdf/>
PREFIX  owl:  <http://www.w3.org/2002/07/owl#>
PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  vcard: <http://www.w3.org/2001/vcard-rdf/3.0#>
PREFIX  t-owl-rstr: <http://candidate.ottr.xyz/owl/restriction/>
PREFIX  p:    <http://www.co-ode.org/ontologies/pizza/pizza.owl#>
PREFIX  rss:  <http://purl.org/rss/1.0/>
PREFIX  t-owl-axiom: <http://candidate.ottr.xyz/owl/axiom/>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  ottr: <http://ns.ottr.xyz/templates#>
PREFIX  ja:   <http://jena.hpl.hp.com/2005/11/Assembler#>
PREFIX  t-rdfs: <http://candidate.ottr.xyz/rdfs/>
PREFIX  t-owl-atom: <http://candidate.ottr.xyz/owl/atom/>
PREFIX  dc:   <http://purl.org/dc/elements/1.1/>
PREFIX  skos:   <http://www.w3.org/2004/02/skos/core#>

SELECT  ?param1 
        (concat("(", concat(GROUP_CONCAT (?param2item; SEPARATOR=' '), ")")) AS ?param2)
        ?param3
        (concat("(", concat(GROUP_CONCAT (?param4item; SEPARATOR=' '), ")")) AS ?param4)
        ?param5
        ?param6
WHERE { 
   ?param1 rdfs:subClassOf+ p:PizzaTopping ;
           rdfs:label ?param2item .
  OPTIONAL {
      ?param1 
           rdfs:subClassOf [ a                   owl:Restriction ;
                             owl:onProperty      p:hasSpiciness ;
                             owl:someValuesFrom  ?param5
                           ] .
  }
  OPTIONAL {
      ?param1 skos:prefLabel ?param3 .
  }
  OPTIONAL {
      ?param1 rdfs:comment ?param4item .
  }
  OPTIONAL {
      ?param1 rdfs:subClassOf ?param6 .
      FILTER (?param6 != p:PizzaTopping && !isBlank(?param6))
  }
} GROUP BY ?param1 ?param3 ?param5 ?param6
#+END_SRC

#+BEGIN_SRC txt :tangle toppings.rq
PREFIX  :     <http://draft.ottr.xyz/pizza/NamedPizza#>
PREFIX  eg:   <http://www.example.org/>
PREFIX  t-rdf: <http://candidate.ottr.xyz/rdf/>
PREFIX  owl:  <http://www.w3.org/2002/07/owl#>
PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  vcard: <http://www.w3.org/2001/vcard-rdf/3.0#>
PREFIX  t-owl-rstr: <http://candidate.ottr.xyz/owl/restriction/>
PREFIX  p:    <http://www.co-ode.org/ontologies/pizza/pizza.owl#>
PREFIX  rss:  <http://purl.org/rss/1.0/>
PREFIX  t-owl-axiom: <http://candidate.ottr.xyz/owl/axiom/>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  ottr: <http://ns.ottr.xyz/templates#>
PREFIX  ja:   <http://jena.hpl.hp.com/2005/11/Assembler#>
PREFIX  t-rdfs: <http://candidate.ottr.xyz/rdfs/>
PREFIX  t-owl-atom: <http://candidate.ottr.xyz/owl/atom/>
PREFIX  dc:   <http://purl.org/dc/elements/1.1/>
PREFIX  skos:   <http://www.w3.org/2004/02/skos/core#>

SELECT  ?param1 
        (concat("(", concat(GROUP_CONCAT (concat(?param2item, concat("^^", LANG(?param2item))); SEPARATOR=' '), ")")) AS ?param2)
        ?param3
        ?param4
        ?param5
WHERE { 
   ?param1 rdfs:subClassOf+ p:PizzaTopping ;
           rdfs:label ?param2item .
  OPTIONAL {
      ?param1 
           rdfs:subClassOf [ a                   owl:Restriction ;
                             owl:onProperty      p:hasSpiciness ;
                             owl:someValuesFrom  ?param4
                           ] .
  }
  OPTIONAL {
      ?param1 skos:prefLabel ?param3 .
  }
  OPTIONAL {
      ?param1 rdfs:comment ?param4item .
  }
  OPTIONAL {
      ?param1 rdfs:subClassOf ?param5 .
      FILTER (?param5 != p:PizzaTopping && !isBlank(?param5))
  }
} GROUP BY ?param1 ?param3 ?param4 ?param5
#+END_SRC


#+BEGIN_SRC sh
wget https://protege.stanford.edu/ontologies/pizza/pizza.owl
#+END_SRC

#+RESULTS:

#+BEGIN_SRC sh
#!/bin/bash
~/Downloads/jena/apache-jena-3.9.0/bin/sparql \
--data pizza.owl \
--query namedpizza.rq
#+END_SRC

#+NAME: pizzas
|-------------------+-----------+---------------------------------------------------------------------------------------------------------------------------------------------------|
| param1            | param2    | toppings                                                                                                                                          |
|-------------------+-----------+---------------------------------------------------------------------------------------------------------------------------------------------------|
| p:Veneziana       | p:Italy   | (p:SultanaTopping p:OnionTopping p:TomatoTopping p:PineKernels p:OliveTopping p:MozzarellaTopping p:CaperTopping)                                 |
| p:AmericanHot     | p:America | (p:HotGreenPepperTopping p:MozzarellaTopping p:JalapenoPepperTopping p:TomatoTopping p:PeperoniSausageTopping)                                    |
| p:Margherita      |           | (p:TomatoTopping p:MozzarellaTopping)                                                                                                             |
| p:FourSeasons     |           | (p:TomatoTopping p:AnchoviesTopping p:MozzarellaTopping p:PeperoniSausageTopping p:CaperTopping p:MushroomTopping p:OliveTopping)                 |
| p:Fiorentina      |           | (p:GarlicTopping p:SpinachTopping p:MozzarellaTopping p:OliveTopping p:TomatoTopping p:ParmesanTopping)                                           |
| p:PrinceCarlo     |           | (p:MozzarellaTopping p:ParmesanTopping p:LeekTopping p:TomatoTopping p:RosemaryTopping)                                                           |
| p:LaReine         |           | (p:MushroomTopping p:MozzarellaTopping p:HamTopping p:OliveTopping p:TomatoTopping)                                                               |
| p:American        | p:America | (p:PeperoniSausageTopping p:TomatoTopping p:MozzarellaTopping)                                                                                    |
| p:Caprina         |           | (p:SundriedTomatoTopping p:GoatsCheeseTopping p:MozzarellaTopping p:TomatoTopping)                                                                |
| p:PolloAdAstra    |           | (p:SweetPepperTopping p:CajunSpiceTopping p:GarlicTopping p:RedOnionTopping p:ChickenTopping p:TomatoTopping p:MozzarellaTopping)                 |
| p:Capricciosa     |           | (p:TomatoTopping p:PeperonataTopping p:HamTopping p:CaperTopping p:MozzarellaTopping p:OliveTopping p:AnchoviesTopping)                           |
| p:Mushroom        |           | (p:TomatoTopping p:MushroomTopping p:MozzarellaTopping)                                                                                           |
| p:FruttiDiMare    |           | (p:TomatoTopping p:GarlicTopping p:MixedSeafoodTopping)                                                                                           |
| p:SloppyGiuseppe  |           | (p:MozzarellaTopping p:GreenPepperTopping p:TomatoTopping p:OnionTopping p:HotSpicedBeefTopping)                                                  |
| p:Cajun           |           | (p:TobascoPepperSauce p:PrawnsTopping p:MozzarellaTopping p:PeperonataTopping p:TomatoTopping p:OnionTopping)                                     |
| p:Napoletana      | p:Italy   | (p:CaperTopping p:TomatoTopping p:OliveTopping p:MozzarellaTopping p:AnchoviesTopping)                                                            |
| p:Soho            |           | (p:ParmesanTopping p:GarlicTopping p:RocketTopping p:TomatoTopping p:MozzarellaTopping p:OliveTopping)                                            |
| p:QuattroFormaggi |           | (p:TomatoTopping p:FourCheesesTopping)                                                                                                            |
| p:Giardiniera     |           | (p:PeperonataTopping p:SlicedTomatoTopping p:TomatoTopping p:MushroomTopping p:LeekTopping p:MozzarellaTopping p:OliveTopping p:PetitPoisTopping) |
| p:Siciliana       |           | (p:AnchoviesTopping p:TomatoTopping p:HamTopping p:OliveTopping p:MozzarellaTopping p:GarlicTopping p:ArtichokeTopping)                           |
| p:Parmense        |           | (p:AsparagusTopping p:ParmesanTopping p:TomatoTopping p:MozzarellaTopping p:HamTopping)                                                           |
| p:Rosa            |           | (p:TomatoTopping p:MozzarellaTopping p:GorgonzolaTopping)                                                                                         |
|-------------------+-----------+---------------------------------------------------------------------------------------------------------------------------------------------------|


#+BEGIN_SRC sh
#!/bin/bash
~/Downloads/jena/apache-jena-3.9.0/bin/sparql \
--data pizza.owl \
--query toppings.rq
#+END_SRC

#+NAME: toppings
|---------------------------+---------------------------------------------------------------------+---------------------------+-----------+----------------------|
| Name                      | Labels                                                              | PrefLanbel                | Spiciness | Super                |
|---------------------------+---------------------------------------------------------------------+---------------------------+-----------+----------------------|
| p:NutTopping              | ('NutTopping'@en 'CoberturaDeCastanha'@pt)                        | 'Nut'@en                  | p:Mild    |                      |
| p:GarlicTopping           | ('GarlicTopping'@en 'CoberturaDeAlho'@pt)                         | 'Garlic'@en               | p:Medium  | p:VegetableTopping   |
| p:PepperTopping           | ('PepperTopping'@en 'CoberturaDePimentao'@pt)                     | 'Pepper'@en               |           | p:VegetableTopping   |
| p:RocketTopping           | ('CoberturaRocket'@pt 'RocketTopping'@en)                         | 'Rocket'@en               | p:Medium  | p:VegetableTopping   |
| p:ArtichokeTopping        | ('CoberturaDeArtichoke'@pt 'ArtichokeTopping'@en)                 | 'Artichoke'@en            | p:Mild    | p:VegetableTopping   |
| p:ParmesanTopping         | ('CoberturaDeParmesao'@pt 'ParmezanTopping'@en)                   | 'Parmezan'@en             | p:Mild    | p:CheeseTopping      |
| p:GoatsCheeseTopping      | ('GoatsCheeseTopping'@en 'CoberturaDeQueijoDeCabra'@pt)           | 'Goats Cheese'@en         | p:Mild    | p:CheeseTopping      |
| p:MozzarellaTopping       | ('CoberturaDeMozzarella'@pt 'MozzarellaTopping'@en)               | 'Mozzarella'@en           | p:Mild    | p:CheeseTopping      |
| p:VegetableTopping        | ('VegetableTopping'@en 'CoberturaDeVegetais'@pt)                  | 'Vegetable Topping'@en    |           |                      |
| p:TomatoTopping           | ('TomatoTopping'@en 'CoberturaDeTomate'@pt)                       | 'Tomato'@en               | p:Mild    | p:VegetableTopping   |
| p:GreenPepperTopping      | ('CoberturaDePimentaoVerde'@pt 'GreenPepperTopping'@en)           | 'Green Pepper'@en         |           | p:PepperTopping      |
| p:HotGreenPepperTopping   | ('CoberturaDePimentaoVerdePicante'@pt 'HotGreenPepperTopping'@en) | 'Hot Green Pepper'@en     | p:Hot     | p:GreenPepperTopping |
| p:HotSpicedBeefTopping    | ('HotSpicedBeefTopping'@en 'CoberturaDeBifePicante'@pt)           | 'Hot Spiced Beef'@en      | p:Hot     | p:MeatTopping        |
| p:RedOnionTopping         | ('RedOnionTopping'@en 'CoberturaDeCebolaVermelha'@pt)             | 'Red Onion'@en            |           | p:OnionTopping       |
| p:GorgonzolaTopping       | ('CoberturaDeGorgonzola'@pt 'GorgonzolaTopping'@en)               | 'Gorgonzola'@en           | p:Mild    | p:CheeseTopping      |
| p:PeperoniSausageTopping  | ('CoberturaDeCalabreza'@pt 'PeperoniSausageTopping'@en)           | 'Peperoni Sausage'@en     | p:Medium  | p:MeatTopping        |
| p:OnionTopping            | ('CoberturaDeCebola'@pt 'OnionTopping'@en)                        | 'Onion'@en                | p:Medium  | p:VegetableTopping   |
| p:PrawnsTopping           | ('CoberturaDeCamarao'@pt 'PrawnsTopping'@en)                      | 'Prawns'@en               |           | p:FishTopping        |
| p:ParmaHamTopping         | ('ParmaHamTopping'@en 'CoberturaDePrezuntoParma'@pt)              | 'Parma Ham'@en            | p:Mild    | p:HamTopping         |
| p:TobascoPepperSauce      | ('MolhoTobascoPepper'@pt 'TobascoPepperSauceTopping'@en)          | 'Tobasco Pepper Sauce'@en | p:Hot     | p:SauceTopping       |
| p:CajunSpiceTopping       | ('CajunSpiceTopping'@en 'CoberturaDeCajun'@pt)                    | 'Cajun Spice'@en          | p:Hot     | p:HerbSpiceTopping   |
| p:HamTopping              | ('HamTopping'@en 'CoberturaDePresunto'@pt)                        | 'Ham'@en                  |           | p:MeatTopping        |
| p:PetitPoisTopping        | ('PetitPoisTopping'@en 'CoberturaPetitPois'@pt)                   | 'Petit Pois'@en           | p:Mild    | p:VegetableTopping   |
| p:SlicedTomatoTopping     | ('SlicedTomatoTopping'@en 'CoberturaDeTomateFatiado'@pt)          | 'Sliced Tomato'@en        | p:Mild    | p:TomatoTopping      |
| p:FishTopping             | ('CoberturaDePeixe'@pt 'SeafoodTopping'@en)                       | 'Seafood'@en              | p:Mild    |                      |
| p:SauceTopping            | ('CoberturaEmMolho'@pt 'SauceTopping'@en)                         | 'Sauce'@en                |           |                      |
| p:MixedSeafoodTopping     | ('MixedSeafoodTopping'@en 'CoberturaDeFrutosDoMarMistos'@pt)      | 'Mixed Seafood'@en        |           | p:FishTopping        |
| p:SundriedTomatoTopping   | ('SundriedTomatoTopping'@en 'CoberturaDeTomateRessecadoAoSol'@pt) | 'Sundried Tomato'@en      | p:Mild    | p:TomatoTopping      |
| p:MushroomTopping         | ('MushroomTopping'@en 'CoberturaDeCogumelo'@pt)                   | 'Mushroom'@en             | p:Mild    | p:VegetableTopping   |
| p:FourCheesesTopping      | ('CoberturaQuatroQueijos'@pt 'FourCheesesTopping'@en)             | 'Four Cheeses'@en         | p:Mild    | p:CheeseTopping      |
| p:ChickenTopping          | ('ChickenTopping'@en 'CoberturaDeFrango'@pt)                      | 'Chicken'@en              | p:Mild    | p:MeatTopping        |
| p:AnchoviesTopping        | ('AnchoviesTopping'@en 'CoberturaDeAnchovies'@pt)                 | 'Anchovies'@en            |           | p:FishTopping        |
| p:SpinachTopping          | ('SpinachTopping'@en 'CoberturaDeEspinafre'@pt)                   | 'Spinach'@en              | p:Mild    | p:VegetableTopping   |
| p:FruitTopping            | ('FruitTopping'@en 'CoberturaDeFrutas'@pt)                        | 'Fruit'@en                |           |                      |
| p:RosemaryTopping         | ('CoberturaRosemary'@pt 'RosemaryTopping'@en)                     | 'Rosemary'@en             | p:Mild    | p:HerbSpiceTopping   |
| p:PeperonataTopping       | ('CoberturaPeperonata'@pt 'PeperonataTopping'@en)                 | 'Peperonata'@en           | p:Medium  | p:PepperTopping      |
| p:PineKernels             | ('CoberturaPineKernels'@pt 'PineKernelTopping'@en)                | 'Pine Kernel'@en          |           | p:NutTopping         |
| p:CaperTopping            | ('CoberturaDeCaper'@pt 'CaperTopping'@en)                         | 'Caper'@en                | p:Mild    | p:VegetableTopping   |
| p:AsparagusTopping        | ('CoberturaDeAspargos'@pt 'AsparagusTopping'@en)                  | 'Asparagus'@en            | p:Mild    | p:VegetableTopping   |
| p:JalapenoPepperTopping   | ('CoberturaDeJalapeno'@pt 'JalapenoPepperTopping'@en)             | 'Jalapeno Pepper'@en      | p:Hot     | p:PepperTopping      |
| p:CheeseyVegetableTopping | ('CheesyVegetableTopping'@en 'CoberturaDeQueijoComVegetais'@pt)   |                           |           | p:VegetableTopping   |
| p:CheeseTopping           | ('CheeseTopping'@en 'CoberturaDeQueijo'@pt)                       | 'Cheese'@en               |           |                      |
| p:CheeseyVegetableTopping | ('CheesyVegetableTopping'@en 'CoberturaDeQueijoComVegetais'@pt)   |                           |           | p:CheeseTopping      |
| p:OliveTopping            | ('CoberturaDeAzeitona'@pt 'OliveTopping'@en)                      | 'Olive'@en                | p:Mild    | p:VegetableTopping   |
| p:MeatTopping             | ('CoberturaDeCarne'@pt 'MeatTopping'@en)                          | 'Meat'@en                 |           |                      |
| p:LeekTopping             | ('LeekTopping'@en 'CoberturaDeLeek'@pt)                           | 'Leek'@en                 | p:Mild    | p:VegetableTopping   |
| p:SweetPepperTopping      | ('SweetPepperTopping'@en 'CoberturaDePimentaoDoce'@pt)            | 'Sweet Pepper'@en         | p:Mild    | p:PepperTopping      |
| p:HerbSpiceTopping        | ('CoberturaDeErvas'@pt 'HerbSpiceTopping'@en)                     | 'Herb Spice'@en           |           |                      |
| p:SultanaTopping          | ('CoberturaSultana'@pt 'SultanaTopping'@en)                       | 'Sultana'@en              | p:Medium  | p:FruitTopping       |
|---------------------------+---------------------------------------------------------------------+---------------------------+-----------+----------------------|

#+BEGIN_SRC txt :tangle toppings.ttl :noweb yes
<<tplinst(template="p:PizzaTopping", instances=toppings)>>
#+END_SRC

#+BEGIN_SRC txt :tangle pizzas.ttl :noweb yes
<<tplinst(template="p:NamedPizza", instances=pizzas)>>
#+END_SRC

