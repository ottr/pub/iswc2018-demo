.PHONY: all build deploy tangle html-export touch clean

null :=
space := ${null} ${null}

%.pdf: %.tex
	cd $(@D); \
	pdflatex $(<F); \
	pdflatex $(<F)

ORGFILES = $(wildcard *.org)
## quoted list of org-files:
ORGFILES-EMACS = $(subst ${space},\" \",$(ORGFILES))

## download latest org-mode
xorgmode:
	git clone https://code.orgmode.org/bzg/org-mode.git $@
	cd $@ ; make autoloads

org-mode:
	wget https://code.orgmode.org/bzg/org-mode/archive/release_9.0.10.zip -O $@.zip
	unzip $@.zip
	cd $@; make autoloads

## run tangle on each org-file
tangle: org-mode setup.emacs $(ORGFILES)
	emacs --batch -l setup.emacs --eval "(progn (mapc (lambda (file)(find-file (expand-file-name file))(org-babel-tangle) (kill-buffer)) '(\"$(ORGFILES-EMACS)\")))"

## export each org-file to html
html-export: org-mode setup.emacs $(ORGFILES) 
	emacs --batch -l setup.emacs --eval "(progn (mapc (lambda (file)(find-file (expand-file-name file))(org-html-export-to-html) (kill-buffer)) '(\"$(ORGFILES-EMACS)\")))"


# main targets:

build: tangle html-export

all: build
	sh run.sh

touch:
	touch $(ORGFILES)

## cleaning

TEMPORGFILES = tex tex~ aux log nav out snm pdf toc vrb
clean:
	rm -Rf org-mode
	$(foreach f, $(ORGFILES:.org=), \
	$(foreach ext, $(TEMPORGFILES), rm -f $(f).$(ext);))
