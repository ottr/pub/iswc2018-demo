exec 2>&1
## write commands too
set -x
printf "\n\n\n"


## download jar files
if [ ! -f ./lutra.jar ];
then
  wget https://gitlab.com/ottr/lutra/lutra/builds/artifacts/v0.1.6/raw/lutra.jar?job=build_tags -O lutra.jar
fi

if [ ! -f ./findPatterns.jar ];
then
  wget http://folk.uio.no/martige/what/20180601/findPatterns.jar -O findPatterns.jar
fi
printf "\n\n\n"


## display CLI
java -jar lutra.jar
printf "\n\n\n"


## convert to stOTTR serialisation
java -jar lutra.jar -stottr -in tpl/intro/SubClassOf1
printf "\n\n\n"


## expand instances
java -jar lutra.jar -expand -in inst/intro/SubClassOf1Instances
printf "\n\n\n"


## expand template definition
java -jar lutra.jar -expand -in tpl/intro/SubClassOf1
printf "\n\n\n"


## types: stOTTR serialisation
java -jar lutra.jar -stottr -in tpl/types/SubClassOf3.ttl -out tpl/types/SubClassOf3.ttl.stottr
cat tpl/types/SubClassOf3.ttl.stottr
printf "\n\n\n"


## type check
java -jar lutra.jar -expand -lib tpl/types -in inst/types/SubClassOf3Instances -out inst/types/SubClassOf3Instances.out
printf "\n\n\n"


## type expansion
cat inst/types/SubClassOf3Instances.out
printf "\n\n\n"


## type error
java -jar lutra.jar -expand -lib tpl/types -in inst/types/SubClassOf3Instances2
printf "\n\n\n"


## cardinality: optional
java -jar lutra.jar -expand -in inst/card/ResourceDescriptionInstances
printf "\n\n\n"


## cardinality: multiple
java -jar lutra.jar -expand -in inst/card/EquivObjectUnionOfInstances
printf "\n\n\n"


## mode test1
java -jar lutra.jar -quiet -expand -lib tpl/mode -in tpl/mode/SubClassOf1Instances
printf "\n\n\n"


## mode test2
java -jar lutra.jar -quiet -expand -lib tpl/mode -in tpl/mode/SubClassOf2Instances
printf "\n\n\n"


## mode test3
java -jar lutra.jar -quiet -expand -lib tpl/mode -in tpl/mode/SubClassOf3Instances
printf "\n\n\n"


## pizza stottr
java -jar lutra.jar -stottr -in http://draft.ottr.xyz/pizza/NamedPizza
printf "\n\n\n"


## pizza instances
java -jar lutra.jar -expand -in tpl/pizza/NamedPizzaInstances
printf "\n\n\n"


## pizza tabOTTR
java -jar lutra.jar -tabottr -in tab/NamedPizza-instances.xlsx
printf "\n\n\n"


## find redundancies, restricted to demo-paper patterns
java -jar findPatterns.jar -p redundancy-templates/
printf "\n\n\n"


## find redundancies, all
java -jar findPatterns.jar -a redundancy-templates/
printf "\n\n\n"
:
