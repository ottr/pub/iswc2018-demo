
(setq debug-on-error t)

;; in case we have a local org-mode copy
(add-to-list 'load-path (expand-file-name "./org-mode/lisp"))
(add-to-list 'load-path (expand-file-name "./org-mode/contrib/lisp" t))

(require 'org)

;;(require 'org-id)
;;(setq org-id-link-to-org-use-id nil)
;;(org-id-update-id-locations)

(require 'ox-latex)
(require 'ob)
(require 'ob-exp)
(require 'ob-tangle)

;; source block languages
(org-babel-do-load-languages 'org-babel-load-languages
    '(
        (shell . t)
    )
)

;; allow all evaluations of source blocks without confirmation:
(setq org-confirm-babel-evaluate nil)

;; direct stderr to stdout
(setq org-babel-default-header-args:sh
'((:prologue . "exec 2>&1") (:epilogue . ":"))
)